from mlxtend.classifier import MultiLayerPerceptron as MLP
from mlxtend.data import loadlocal_mnist
from mlxtend.preprocessing import shuffle_arrays_unison
from mlxtend.preprocessing import standardize
from openpyxl import load_workbook
from datetime import date
import random

log_data = './log/log_data.xlsx'  # PLANILHA LOCAL PARA SALVAR OS DADOS
wb = load_workbook(filename=log_data)  # NOME DA "FOLHA" DA PLANILHA
ws = wb['Planilha1']

# número de camadas ocultas, dá para dar uma experimentada com mais camadas, ao invés de apenas quantia
hidden_layers_setup = 6
l2_setup = 0.0
l1_setup = 0.0
# vezes que repete o processo de treino com os 60K (quanto mais epochs, melhor)
epochs_setup = 5
# taxa de aprendizagem (quanto menor melhor deu resultado)
eta_setup = 0.1
momentum_setup = 0.0
decrease_const_setup = 0.0
# como o conjunto de treino tem 60K de dígitos, escolhi esse valor
minibatches_setup = 100
# para iniciar os pesos e biases com valores aleatórios
random_seed_setup = 1
print_progress_setup = 3

# eu ando usando a classe para carregar dados locais, mas tem a classe padrão que não usar arquivo local
# no caso, você vai ter que baixar os arquivos no site do MNIST e depois descompactar os arquivos .gz
X_treino, y_treino = loadlocal_mnist(
    images_path='./data/train-images.idx3-ubyte',
    labels_path='./data/train-labels.idx1-ubyte'
)
X_treino, y_treino = shuffle_arrays_unison(
    (X_treino, y_treino), random_seed=1)  # embaralha os valores dos dois arrays

X_teste, y_teste = loadlocal_mnist(
    images_path='./data/t10k-images.idx3-ubyte',
    labels_path='./data/t10k-labels.idx1-ubyte'
)
X_teste, y_teste = shuffle_arrays_unison((X_teste, y_teste), random_seed=1)

# alguns atributos não sei o que fazem, mas vou marcar os que sei que alteram o resultado
net = MLP(
    hidden_layers=[hidden_layers_setup],
    l2=l2_setup,
    l1=l1_setup,
    epochs=epochs_setup,
    eta=eta_setup,
    momentum=momentum_setup,
    decrease_const=decrease_const_setup,
    minibatches=minibatches_setup,
    random_seed=random_seed_setup,
    print_progress=print_progress_setup
)

# padronização (tradução certa?) para fazer os valores dos pixel que são de 0 a 255 tornarem-se de 0 a 1
X_treino_std, params = standardize(
    X_treino,
    columns=range(X_treino.shape[1]),
    return_params=True
)

X_teste_std = standardize(
    X_teste,
    columns=range(X_teste.shape[1]),
    params=params
)

def logxlsx():
    ultimaLinha = ws.max_row + 1    # PEGA A ULTIMA LINHA COM DADOS E ADD + 1 (AQUI QUE DEVE TA DANDO PROBLEMA)
    dataAtual = date.today()
    # ABAIXO SETA VALORES NAS COLUNAS DA PLANILHA XLSX
    ws.cell(column=1, row=ultimaLinha, value=dataAtual)
    ws.cell(column=2, row=ultimaLinha, value=100*net.score(X_treino_std, y_treino))
    ws.cell(column=3, row=ultimaLinha, value=100*net.score(X_teste_std, y_teste))
    ws.cell(column=4, row=ultimaLinha, value=hidden_layers_setup)
    ws.cell(column=5, row=ultimaLinha, value=l2_setup)
    ws.cell(column=6, row=ultimaLinha, value=l1_setup)
    ws.cell(column=7, row=ultimaLinha, value=epochs_setup)
    ws.cell(column=8, row=ultimaLinha, value=eta_setup)
    ws.cell(column=9, row=ultimaLinha, value=momentum_setup)
    ws.cell(column=10, row=ultimaLinha, value=decrease_const_setup)
    ws.cell(column=11, row=ultimaLinha, value=minibatches_setup)
    ws.cell(column=12, row=ultimaLinha, value=random_seed_setup)
    ws.cell(column=13, row=ultimaLinha, value=print_progress_setup)
    wb.save(filename=log_data)
    wb.close()

def main():
    net.fit(X_treino_std, y_treino)  # faz o treino

    print('\n\nPrecisão Treino: %.2f%% | Precisão de teste: %.2f%%' %
          ((100 * net.score(X_treino_std, y_treino)),
           (100 * net.score(X_teste_std, y_teste))))

    logxlsx()

    X_rand = X_teste[9999:]
    print('Dimensions: %s x %s' % (X_rand.shape[0], X_rand.shape[1]))

    img = X_rand[0].reshape(28, 28)

    print('Seu resultado foi: %d' % net.predict(standardize(X_rand, columns=range(X_rand.shape[1]), params=params)))
    X_rand[0] = [random.randint(0, 255) for x in X_rand[0]]

main()